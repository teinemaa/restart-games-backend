#!/bin/bash
dpkg --add-architecture i386
apt-get update
apt-get -y install curl wget file bzip2 gzip unzip python util-linux tmux {{libraries}}
adduser --disabled-password --gecos "" {{game}}
su - {{game}} -c 'wget https://gameservermanagers.com/dl/{{game}} ; chmod +x {{game}} ; ./{{game}} auto-install'