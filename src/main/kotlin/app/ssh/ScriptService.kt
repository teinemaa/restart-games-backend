package app.ssh

import app.game.Game

interface ScriptService {
    fun run(game: Game, action: String)
}