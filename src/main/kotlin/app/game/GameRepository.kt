package app.game

import app.cloud.NodeApi
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
class GameRepository @Autowired constructor(private val gameRepository: GameEntityRepository, private val nodeApi: NodeApi){
    fun save(user: String, game: Game): Game = gameRepository.save(game.toEntity(user)).toGame()

    fun findOne(user: String, id: String): Game = gameRepository.findOne(id).toGame()

    fun findAll(user: String): Iterable<Game> = gameRepository.findAll()
            .filter { it.user == user }
            .map { it.toGame() }

    fun create(user: String, type: GameType, name: String): Game {
        val newGame = GameEntity(user, name, type, Game.Status.RUNNING, Game.Condition.BUZY)
        return gameRepository.save(newGame).toGame()
    }

}

private fun GameEntity.toGame() = Game(id,type,name,status,condition,ipAddress,shutdown)

private fun Game.toEntity(user: String) = GameEntity(
        user = user,
        name = name,
        type = type,
        status = status,
        condition = condition,
        ipAddress = ipAddress,
        id = id,
        shutdown = shutdown
)

@Repository
interface GameEntityRepository : CrudRepository<GameEntity, String>