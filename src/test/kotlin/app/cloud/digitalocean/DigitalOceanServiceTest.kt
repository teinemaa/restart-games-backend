package app.cloud.digitalocean

import app.ssh.SshKeys
import app.util.Async
import com.myjeeva.digitalocean.DigitalOcean
import com.myjeeva.digitalocean.common.ActionStatus.COMPLETED
import com.myjeeva.digitalocean.common.ActionStatus.IN_PROGRESS
import com.myjeeva.digitalocean.pojo.*
import com.nhaarman.mockito_kotlin.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test

class DigitalOceanServiceTest {

    lateinit var service: DigitalOceanService
    lateinit var async: Async
    lateinit var digitalOcean: DigitalOcean

    @Before
    fun setUp() {
        digitalOcean = mock<DigitalOcean> {
            on { getImageInfo("debian-8-x64") } doReturn
                    Image().apply { name = "debian" }
            on { getAvailableRegions(1) } doReturn Regions().apply {
                regions = listOf(
                        Region().apply { slug = "ams3"; name = "amsterdam 3" }) }
            on { getAvailableKeys(0) } doReturn Keys().apply {
                keys = listOf(
                        Key().apply { fingerprint = "fp1"; name = "key1" },
                        Key().apply { fingerprint = "fp2" },
                        Key().apply { fingerprint = "fp3"; name = "key3" }) }
            on { createDroplet(any<Droplet>()) } doReturn
                    Droplet().apply { id = 12 }
            on { getAvailableDropletActions(12, 1, null) } doReturn Actions().apply {
                actions = listOf(
                        Action().apply { id = 21; status = IN_PROGRESS }) }
            on { getActionInfo(21) } doReturn
                    Action().apply { status = COMPLETED }
            on { getDropletInfo(12) } doReturn listOf(
                    Droplet().apply {
                        id = 12
                        name = "game-hosting-612"
                        networks = Networks().apply { version4Networks = emptyList() } },
                    Droplet().apply {
                        name = "game-hosting-612"
                        networks = Networks().apply { version4Networks = listOf(
                                Network().apply { ipAddress = "12.23.14.34" }) } })
            on { getAvailableDroplets(0, null) } doReturn Droplets().apply { droplets = listOf(
                    Droplet().apply {
                        id = 53
                        name = "game-hosting-543"
                        networks = Networks().apply { version4Networks = listOf(
                                Network().apply { ipAddress = "75.86.72.51" }) } },
                    Droplet().apply {
                        name = "server-hosting-23" },
                    Droplet().apply {
                        id = 88
                        name = "game-hosting-143"
                        networks = Networks().apply { version4Networks = listOf(
                                Network().apply { ipAddress = "97.45.35.13" }) } },
                    Droplet().apply {
                        name = "game-hosting-721"
                        networks = Networks().apply { version4Networks = emptyList() } }) }
            on { shutdownDroplet(53) } doReturn
                    Action().apply { id = 5; status = IN_PROGRESS }
            on { getActionInfo(5) } doReturn
                    Action().apply { status = COMPLETED }
            on { takeDropletSnapshot(53, "game-hosting-543") } doReturn
                    Action().apply { id = 4; status = IN_PROGRESS }
            on { getActionInfo(4) } doReturn
                    Action().apply { status = COMPLETED }
            on { deleteDroplet(53) } doReturn
                    Delete().apply { isRequestSuccess = true }
            on { getAvailableSnapshots(0, null) } doReturn Snapshots().apply {
                snapshots = listOf(
                        Snapshot().apply { name = "snapshot-123" },
                        Snapshot().apply { name = "game-hosting-543" },
                        Snapshot().apply { id = 73; name = "game-hosting-612" }) }
            on { deleteSnapshot("73") } doReturn
                    Delete().apply { isRequestSuccess = true }
        }
        val sshKeys = mock<SshKeys>() {
            on { fingerprints() } doReturn listOf("fp1", "fp3")
        }
        async = mock<Async>()
        service = DigitalOceanService(digitalOcean, sshKeys, async)
    }

    @Test
    fun createDroplet() {
        service.create("612").apply {
            assertThat(id).isEqualTo("612")
            assertThat(ipAddress).isEqualTo("12.23.14.34")
        }

        inOrder(digitalOcean, async) {
            argumentCaptor<Droplet>().apply {
                verify(digitalOcean).createDroplet(capture())
                assertThat(firstValue.name).isEqualTo("game-hosting-612")
                assertThat(firstValue.image.name).isEqualTo("debian")
                assertThat(firstValue.region.name).isEqualTo("amsterdam 3")
                assertThat(firstValue.size).isEqualTo("512mb")
                assertThat(firstValue.keys.map { it.name }).containsExactly("key1", "key3")
            }
            verify(async).delay(5000)
            verify(async).delay(5000)
            verifyNoMoreInteractions(async)
        }
    }

    @Test
    fun stopDroplet() {
        service.stop("543").apply {
            assertThat(id).isEqualTo("543")
        }

        inOrder(digitalOcean, async) {
            verify(digitalOcean).shutdownDroplet(53)
            verify(async).delay(5000)
            verify(digitalOcean).takeDropletSnapshot(53, "game-hosting-543")
            verify(async).delay(5000)
            verify(digitalOcean).deleteDroplet(53)
            verifyNoMoreInteractions(async)
        }
    }

    @Test
    fun resumeDroplet() {
        service.resume("612").apply {
            assertThat(id).isEqualTo("612")
            assertThat(ipAddress).isEqualTo("12.23.14.34")
        }

        inOrder(digitalOcean, async) {
            argumentCaptor<Droplet>().apply {
                verify(digitalOcean).createDroplet(capture())
                assertThat(firstValue.name).isEqualTo("game-hosting-612")
                assertThat(firstValue.image.name).isEqualTo("game-hosting-612")
                assertThat(firstValue.region.name).isEqualTo("amsterdam 3")
                assertThat(firstValue.size).isEqualTo("512mb")
                assertThat(firstValue.keys.map { it.name }).containsExactly("key1", "key3")
            }
            verify(async).delay(5000)
            verify(digitalOcean).deleteSnapshot("73")
            verify(async).delay(5000)
            verifyNoMoreInteractions(async)
        }
    }

    @Test
    fun deleteDroplet() {
        service.deleteNode("543")

        verify(digitalOcean).deleteDroplet(53)
        verifyNoMoreInteractions(async)
    }

    @Test
    fun deleteSnapshot() {
        service.deleteSnapshot("612")

        verify(digitalOcean).deleteSnapshot("73")
        verifyNoMoreInteractions(async)
    }

    @Test
    fun snapshots() {
        service.snapshots.apply {
            assertThat(this.map { it.id }).containsExactly("543", "612")
        }

        verifyNoMoreInteractions(async)
    }

    @Test
    fun droplets() {
        service.nodes.apply {
            assertThat(this.map { it.id }).containsExactly("543", "143")
            assertThat(this.map { it.ipAddress }).containsExactly("75.86.72.51", "97.45.35.13")
        }

        verifyNoMoreInteractions(async)
    }
}


