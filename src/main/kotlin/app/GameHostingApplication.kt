package app

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.scheduling.annotation.EnableAsync

@SpringBootApplication
@EnableAsync
class GameHostingApplication

fun main(args: Array<String>) {
    SpringApplication.run(GameHostingApplication::class.java, *args)

}
