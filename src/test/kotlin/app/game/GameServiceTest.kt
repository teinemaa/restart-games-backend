package app.game

import app.cloud.Node
import app.cloud.NodeApi
import app.game.Game.Condition.*
import app.game.Game.Status.*
import app.game.GameType.factorio
import app.ssh.ScriptService
import app.util.Async
import com.nhaarman.mockito_kotlin.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.AdditionalAnswers.returnsSecondArg
import org.mockito.InOrder
import org.mockito.Matchers.anyString
import org.slf4j.Logger

class GameServiceTest {

    lateinit var service: GameService
    lateinit var async: Async
    lateinit var gameRepository: GameRepository
    lateinit var nodeApi: NodeApi
    lateinit var sshApi: ScriptService

    val createdGame = Game(id = "2312", type = factorio, name = "factorio 123", status = RUNNING, condition = BUZY)
    val runningGame = Game(id = "4123", type = factorio, name = "factorio 456", status = RUNNING, condition = READY, ipAddress = "12.34.23.75")
    val savedBuzyGame = runningGame.copy(status = SAVED, condition = BUZY)
    val savedGame = Game(id = "7682", type = factorio, name = "factorio 789", status = SAVED, condition = READY)

    val runningBuzyGame = savedGame.copy(status = RUNNING, condition = BUZY)

    @Before
    fun setUp() {
        nodeApi = mock<NodeApi>() {
            on { create("2312") } doReturn Node("2312", "52.12.33.23")
            on { resume("7682") } doReturn Node("7682", "87.67.15.42")
            on { stop("7682") } doThrow RuntimeException()
        }
        sshApi = mock<ScriptService>()
        gameRepository = mock<GameRepository>() {
            on { create("john", factorio, "factorio 123") } doReturn createdGame
            on { findOne("john", "4123") } doReturn runningGame
            on { findOne("john", "7682") } doReturn savedGame
            on { save(eq("john"), any()) } doAnswer { returnsSecondArg<Game>().answer(it) }
            on { findAll("john") } doReturn listOf(createdGame, savedGame, runningGame)
        }
        async = mock<Async>() {
            on { launch(any()) } doAnswer { (it.arguments[0] as (() -> Unit)).invoke() }
        }
        service = GameService(nodeApi, sshApi, gameRepository, async)
        service.logger = mock<Logger>()
    }

    @Test
    fun start() {
        service.start("john", factorio, "factorio 123")

        exactlyInOrder {
            verify(gameRepository).create("john", factorio, "factorio 123")
            verify(async).launch(any())
            verify(nodeApi).create("2312")
            verify(sshApi).run(createdGame.copy(ipAddress = "52.12.33.23"), "create")
            verify(sshApi).run(createdGame.copy(ipAddress = "52.12.33.23"), "resume")
            verify(gameRepository).save("john", createdGame.copy(ipAddress = "52.12.33.23", condition = READY))
        }
    }

    @Test
    fun save() {
        service.save("john", "4123")

        exactlyInOrder {
            verify(gameRepository).findOne("john", "4123")
            verify(gameRepository).save("john", savedBuzyGame)
            verify(async).launch(any())
            verify(sshApi).run(savedBuzyGame, "save")
            verify(nodeApi).stop("4123")
            verify(gameRepository).save("john", savedBuzyGame.copy(condition = READY, ipAddress = null))
        }
    }

    @Test
    fun resume() {
        service.resume("john", "7682")

        exactlyInOrder {
            verify(gameRepository).findOne("john", "7682")
            verify(gameRepository).save("john", runningBuzyGame)
            verify(async).launch(any())
            verify(nodeApi).resume("7682")
            verify(sshApi).run(runningBuzyGame.copy(ipAddress = "87.67.15.42"), "resume")
            verify(gameRepository).save("john", runningBuzyGame.copy(condition = READY, ipAddress = "87.67.15.42"))
        }
    }

    @Test
    fun deleteGame() {
        service.delete("john", "4123")

        exactlyInOrder {
            verify(gameRepository).findOne("john", "4123")
            verify(gameRepository).save("john", runningGame.copy(status = DELETED, condition = BUZY))
            verify(async).launch(any())
            verify(nodeApi).deleteNode("4123")
            verify(gameRepository).save("john", runningGame.copy(status = DELETED, condition = READY, ipAddress = null))
        }
    }

    @Test
    fun deleteSave() {
        service.delete("john", "7682")

        exactlyInOrder {
            verify(gameRepository).findOne("john", "7682")
            verify(gameRepository).save("john", savedGame.copy(status = DELETED, condition = BUZY))
            verify(async).launch(any())
            verify(nodeApi).deleteSnapshot("7682")
            verify(gameRepository).save("john", savedGame.copy(status = DELETED, condition = READY))
        }
    }

    @Test
    fun getGames() {
        assertThat(service.games("john")).containsExactlyInAnyOrder(createdGame, runningGame, savedGame)

        exactlyInOrder {
            verify(gameRepository).findAll("john")
        }
    }

    @Test
    fun handleFailure() {
        service.save("john", "7682")
        exactlyInOrder {
            verify(gameRepository).findOne("john", "7682")
            verify(gameRepository).save("john", savedGame.copy(condition = BUZY))
            verify(async).launch(any())
            verify(sshApi).run(savedGame.copy(condition = BUZY), "save")
            verify(nodeApi).stop("7682")
            verify(service.logger).error(anyString(), any<Exception>())
            verify(gameRepository).save("john", savedGame.copy(condition = ERROR))
        }
    }

    private fun exactlyInOrder(evaluation: InOrder.() -> Unit) {
        val mocks = arrayOf(gameRepository, nodeApi, sshApi, async, service.logger)
        inOrder(*mocks) {
            evaluation()
            verifyNoMoreInteractions(*mocks)
        }
    }
}
