package app.cloud.digitalocean

import com.myjeeva.digitalocean.impl.DigitalOceanClient
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class DigitalOceanClientProvider {

    @Bean
    fun digitalOceanClient(): DigitalOceanClient {
        return DigitalOceanClient("75c5ca3ad48b10018be6c5876fb0e6474ea113a9fd3e3bcf44da5dc5ad2e3bd3")
    }
}
