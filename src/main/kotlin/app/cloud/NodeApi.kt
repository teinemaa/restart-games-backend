package app.cloud

interface NodeApi {
    fun create(nodeId: String): Node
    fun stop(nodeId: String): Node
    fun resume(snapshotId: String): Node
    fun deleteSnapshot(snapshotId: String)
    val nodes: Collection<Node>
    val snapshots: Collection<Node>
    fun deleteNode(nodeId: String)
}
