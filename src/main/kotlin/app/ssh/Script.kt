package app.ssh

import java.io.InputStream

data class Script(val name: String, val content: String)