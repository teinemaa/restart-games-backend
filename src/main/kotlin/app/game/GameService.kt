package app.game

import app.cloud.NodeApi
import app.game.Game.Condition.*
import app.game.Game.Status
import app.game.Game.Status.*
import app.ssh.ScriptService
import app.util.Async
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.Instant
import java.time.temporal.ChronoUnit

@Service
class GameService @Autowired constructor(
        private val nodeApi: NodeApi,
        private val sshApi: ScriptService,
        private val gameRepository: GameRepository,
        private val async: Async) {

    var logger = LoggerFactory.getLogger(GameService::class.java)

    fun start(user: String, type: GameType, name: String): Game {
        val newGame = gameRepository.create(user, type, name)
        saveAfter(user, newGame) { game: Game ->
            val node = nodeApi.create(game.id)
            val gameWithIp = game.copy(
                    ipAddress = node.ipAddress,
                    shutdown = Instant.now().plus(6, ChronoUnit.HOURS))
            sshApi.run(gameWithIp, "create")
            sshApi.run(gameWithIp, "resume")
            gameWithIp
        }
        return newGame
    }

    fun save(user: String, id: String) {
        val runningGame = gameRepository.findOne(user, id)
        val busyGame = runningGame.saveAs(user, SAVED)
        saveAfter(user, busyGame) { game: Game ->
            sshApi.run(game, "save")
            val stoppedGame = game.copy(ipAddress = null,
                    shutdown = null)
            nodeApi.stop(stoppedGame.id)
            stoppedGame
        }
    }

    fun resume(user: String, id: String) {
        val savedGame = gameRepository.findOne(user, id)
        val buzyGame = savedGame.saveAs(user, RUNNING)
        saveAfter(user, buzyGame) { game: Game ->
            val node = nodeApi.resume(game.id)
            val gameWithIp = game.copy(ipAddress = node.ipAddress,
                    shutdown = Instant.now().plus(6, ChronoUnit.HOURS))
            sshApi.run(gameWithIp, "resume")
            gameWithIp
        }
    }

    fun delete(user: String, id: String) {
        val game = gameRepository.findOne(user, id)
        val busyGame = game.saveAs(user, DELETED)
        saveAfter(user, busyGame, if (game.running) deleteGame() else deleteSave())
    }

    fun extendTimer(user: String, id: String) {
        val gameToExtend = gameRepository.findOne(user, id)
        val extendedGame = gameToExtend.copy(shutdown = Instant.now().plus(6, ChronoUnit.HOURS))
        gameRepository.save(user, extendedGame)
    }

    fun games(user: String) = gameRepository.findAll(user)

    private fun deleteGame() = { game: Game ->
        nodeApi.deleteNode(game.id)
        game.copy(ipAddress = null, shutdown = null)
    }

    private fun deleteSave() = { game: Game ->
        nodeApi.deleteSnapshot(game.id)
        game
    }

    private fun Game.saveAs(user: String, status: Status) = gameRepository.save(user, this.copy(condition = BUZY, status = status))

    private fun saveAfter(user: String, game: Game, action: (Game) -> Game) {
        async.launch {
            var toBeSaved = game
            try {
                toBeSaved = action.invoke(game)
                toBeSaved = toBeSaved.copy(condition = READY)
            } catch (e: Exception) {
                toBeSaved = game.copy(condition = ERROR)
                logger.error("operation with game failed: ", e)
            } finally {
                gameRepository.save(user, toBeSaved)
            }
        }
    }
}

