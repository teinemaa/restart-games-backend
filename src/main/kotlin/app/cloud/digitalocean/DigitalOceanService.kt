package app.cloud.digitalocean

import app.cloud.Node
import app.cloud.NodeApi
import app.ssh.SshKeys
import app.util.Async
import com.myjeeva.digitalocean.DigitalOcean
import com.myjeeva.digitalocean.common.ActionStatus
import com.myjeeva.digitalocean.pojo.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Service

@Profile("!demo")
@Service
class DigitalOceanService @Autowired constructor(
        private val api: DigitalOcean,
        private val keys: SshKeys,
        private val async: Async) : NodeApi {

    override fun create(id: String): Node {
        return ensureCreated(api.createDroplet(newConf(id))).ensureIpAddress().toNode()
    }

    override fun stop(nodeId: String): Node {
        val droplet = droplet(nodeId)
        ensureCompleted(api.shutdownDroplet(droplet.id))
        ensureCompleted(api.takeDropletSnapshot(droplet.id, droplet.name))
        ensureDeleted(api.deleteDroplet(droplet.id))
        return snapshot(nodeId).toNode()
    }

    override fun resume(snapshotId: String): Node {
        val snapshot = snapshot(snapshotId)
        val droplet = ensureCreated(api.createDroplet(resumeConf(snapshot)))
        ensureDeleted(api.deleteSnapshot(snapshot.id.toString()))
        return droplet.ensureIpAddress().toNode()
    }

    override fun deleteSnapshot(snapshotId: String) {
        api.deleteSnapshot(snapshot(snapshotId).id.toString())
    }

    override fun deleteNode(nodeId: String) {
        api.deleteDroplet(droplet(nodeId).id)
    }

    override val nodes: Collection<Node>
        get() = droplets.map { it.toNode() }

    override val snapshots: Collection<Node>
        get() = dropletSnapshtos.map { it.toNode() }

    private fun ensureDeleted(delete: Delete) {
        if (!delete.isRequestSuccess) {
            throw Exception("deleteSnapshot failed")
        }
    }

    private fun droplet(id: String) = droplets.single { it.name.endsWith(id) }

    private fun resumeConf(snapshot: Snapshot): Droplet {
        val conf = defaultConf()
        conf.image = snapshot
        conf.name = snapshot.name
        return conf
    }

    private fun snapshot(id: String) = dropletSnapshtos.single { it.name.endsWith(id) }

    private val droplets: Collection<Droplet>
        get() = api.getAvailableDroplets(0, null).droplets.filter { it.name.isDropletName() }.filter { it.hasIpAddress }

    private val dropletSnapshtos: Collection<Snapshot>
        get() = api.getAvailableSnapshots(0, null).snapshots.filter { it.name.isDropletName() }

    private fun ensureCompleted(action: Action) {
        var currentAction = action
        while (currentAction.status != ActionStatus.COMPLETED) {
            if (currentAction.status == ActionStatus.ERRORED) throw RuntimeException("Action id %s failed".format(currentAction.id))
            async.delay(5000)
            currentAction = api.getActionInfo(action.id)
        }
    }

    private fun newConf(id: String): Droplet {
        val conf = defaultConf()
        conf.name = dropletName(id)
        return conf
    }

    private fun ensureCreated(droplet: Droplet): Droplet {
        ensureCompleted(api.getAvailableDropletActions(droplet.id, 1, null).actions[0])
        return api.getDropletInfo(droplet.id)
    }

    private fun defaultConf(): Droplet {
        val conf = Droplet()
        conf.image = image
        conf.region = region
        conf.size = "512mb"
        conf.keys = getKeys()
        return conf
    }

    private fun getKeys(): List<Key>
            = api.getAvailableKeys(0).keys.filter { key -> keys.fingerprints().contains(key.fingerprint) }

    private val image: Image
        get() = api.getImageInfo("debian-8-x64")

    private val region: Region
        get() = api.getAvailableRegions(1).regions.filter { region -> region.slug == "ams3" }.first()

    private fun Droplet.ensureIpAddress(): Droplet {
        var droplet = this
        while (!droplet.hasIpAddress) {
            async.delay(5000)
            droplet = api.getDropletInfo(droplet.id)
        }
        return droplet
    }
}

private val Droplet.hasIpAddress: Boolean
    get() = !this.networks.version4Networks.isEmpty()

private fun Droplet.toNode(): Node = Node(
        id = name.nameToId(),
        ipAddress = networks.version4Networks[0].ipAddress)

private fun Snapshot.toNode(): Node = Node(
        id = name.nameToId())

private fun dropletName(id: String): String = "game-hosting-%s".format(id)

private fun String.dropletNameRegex() = dropletName("(.*)").toRegex().matchEntire(this)

private fun String.isDropletName(): Boolean = dropletNameRegex() != null

private fun String.nameToId(): String = dropletNameRegex()!!.groupValues[1]
