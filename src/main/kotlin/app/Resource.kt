package app

interface Resource {
    val id: String
}

fun <T> Iterable<T>.byId(id: String): T where T : Resource = this.single { id == it.id }