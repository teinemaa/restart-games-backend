package app.cloud

import app.Resource

data class Node(
        override val id: String,
        val ipAddress: String? = null
) : Resource