package app.db

import org.springframework.boot.autoconfigure.flyway.FlywayMigrationStrategy
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class MigrationStrategy {
    @Bean
    fun cleanMigrateStrategy(): FlywayMigrationStrategy {
        val strategy = FlywayMigrationStrategy { flyway ->
            flyway.clean()
            flyway.migrate()
        }
        return strategy
    }
}