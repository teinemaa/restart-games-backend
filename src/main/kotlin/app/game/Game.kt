package app.game

import app.Resource
import app.game.Game.Status.RUNNING
import app.game.Game.Status.SAVED
import java.time.Instant

data class Game(
        override val id: String,
        val type: GameType,
        val name: String,
        val status: Status,
        val condition: Condition,
        val ipAddress: String? = null,
        val shutdown: Instant? = null
) : Resource {
    enum class Status {
        RUNNING,
        SAVED,
        DELETED
    }
    enum class Condition {
        READY,
        BUZY,
        ERROR
    }
}

val Game.running: Boolean
    get() = this.status == RUNNING

val Game.saved: Boolean
    get() = this.status == SAVED

val Game.busy: Boolean
    get() = this.condition == Game.Condition.BUZY

val Game.error: Boolean
    get() = this.condition == Game.Condition.ERROR

val Game.ready: Boolean
    get() = this.condition == Game.Condition.READY

