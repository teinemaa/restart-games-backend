CREATE TABLE game (
  id TEXT PRIMARY KEY,
  name TEXT NOT NULL,
  type TEXT NOT NULL,
  state TEXT NOT NULL,
  status TEXT NOT NULL,
  ip_address TEXT,
  shutdown TIMESTAMP
)