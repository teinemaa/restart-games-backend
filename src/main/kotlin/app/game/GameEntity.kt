package app.game

import java.time.Instant
import java.util.*
import javax.persistence.Entity
import javax.persistence.EnumType.STRING
import javax.persistence.Enumerated
import javax.persistence.Id

@Entity(name = "game")
class GameEntity (
        val user: String,

        val name: String,

        @Enumerated(STRING) val type: GameType,

        @Enumerated(STRING) var status: Game.Status,

        @Enumerated(STRING) var condition: Game.Condition,

        var ipAddress: String? = null,

        var shutdown: Instant? = null,

        @Id val id: String = UUID.randomUUID().toString()

)


