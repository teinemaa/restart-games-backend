package app.ssh

import app.game.Game
import com.jcabi.log.Logger
import com.jcabi.ssh.SSH
import com.jcabi.ssh.Shell
import com.jcabi.ssh.Shell.Safe
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.io.IOException
import java.io.InputStream
import java.util.logging.Level.FINE

@Component
class SshExecutor @Autowired constructor(private val keys: SshKeys) {

    private val logger = LoggerFactory.getLogger(SshExecutor::class.java)

    fun executeScript(game: Game, script: Script) {
        val shell = Safe(SSH(game.ipAddress, 22, "root", keys.privateKey()))
        var scriptContent = script.content.replace("\\r?\\n".toRegex(), "\n")
        logger.debug("Executing script %s on %s:\n%s".format(script.name, game.ipAddress, scriptContent))
        shell.ensureExecuted("cat > %s.sh ; chmod +x %<s.sh ; ./%<s.sh".format(script.name), scriptContent.byteInputStream())
    }

    private fun Shell.ensureExecuted(command: String, stdin: InputStream? = null) {
        while (true) {
            try {
                this.exec(command, stdin, Logger.stream(FINE, this), Logger.stream(FINE, this))
                return
            } catch (retry: IOException) {
                logger.debug("Retry after: %s".format(retry))
                Thread.sleep(5000)
            }
        }
    }
}
