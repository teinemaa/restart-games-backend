package app

import app.game.*
import app.game.Game.Status.DELETED
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.time.Instant
import java.time.temporal.ChronoUnit

@RestController
@RequestMapping("api/games")
class Games @Autowired constructor(private val gameService: GameService) {

    @PostMapping()
    fun create(@RequestHeader(value="user") user: String, @RequestBody game: NewGame)
            = gameService.start(user, game.type, gameService.games(user).count().toString()).toView()

    @PatchMapping("{id}")
    fun update(@RequestHeader(value="user") user: String, @RequestBody game: ChangedGame) {
        if (game.shutdown != null && !game.shutdown) gameService.extendTimer(user, game.id)
        if (game.running != null) {
            if (game.running) {
                gameService.resume(user, game.id)
            } else {
                gameService.save(user, game.id)
            }
        }
    }

    @DeleteMapping("{id}")
    fun deleteGame(@RequestHeader(value="user") user: String, @PathVariable id: String) = gameService.delete(user, id)

    @GetMapping()
    fun games(@RequestHeader(value="user", required = false) user: String?): List<GameView> {
        if (user == null ) {
            return emptyList()
        } else {
            return gameService.games(user).filter { it.status != DELETED }.sortedBy { it.name.toInt() }.map(Game::toView)
        }
    }
}

data class NewGame(
        val type: GameType
)

data class ChangedGame (
        val id: String,
        val running: Boolean?,
        val shutdown: Boolean?
)

data class GameView (
        val id: String,
        val name: String,
        val typeDescription: String,
        val ipAddress: String?,
        val busy: Boolean,
        val error: Boolean,
        val running: Boolean,
        val saved: Boolean,
        val ready: Boolean,
        val shutdownInSeconds: Long?
)

private fun Game.toView() = GameView(
        id = id,
        name = name,
        typeDescription = type.title,
        ipAddress = ipAddress,
        busy = busy,
        error = error,
        ready = ready,
        running = ready && running,
        saved = ready && saved,
        shutdownInSeconds = if (shutdown!=null) ChronoUnit.SECONDS.between(Instant.now(), shutdown) else null
        )

