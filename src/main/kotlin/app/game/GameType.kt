package app.game

enum class GameType(val title: String, val code: String, val libraries: String) {
    factorio ("Factorio", "fctr", "lib32gcc1 libstdc++6 libstdc++6:i386"),
    terraria ("Terraria", "terraria", "lib32gcc1 libstdc++6 libstdc++6:i386")
}
