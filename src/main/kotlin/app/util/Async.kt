package app.util

import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Component

@Component
class Async {

    fun delay(millis: Long) {
        Thread.sleep(millis)
    }

    @Async
    fun launch(runnable: () -> Unit) {
        runnable.invoke()
    }
}
