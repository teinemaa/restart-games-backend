package app.ssh

import app.game.Game
import com.samskivert.mustache.Mustache
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component

@Profile("!demo")
@Component
class SshScriptService @Autowired constructor(private val executor: SshExecutor): ScriptService {

    override fun run(game: Game, action: String) {
        val template = this.javaClass.getResourceAsStream("/scripts/%s.sh".format(action))
        var context = mapOf<String, String>(
                "game" to game.type.code + "server",
                "ip" to game.ipAddress!!,
                "libraries" to game.type.libraries)
        val content = Mustache.compiler().compile(template.reader()).execute(context)
        executor.executeScript(game, Script(action, content))
    }
}

