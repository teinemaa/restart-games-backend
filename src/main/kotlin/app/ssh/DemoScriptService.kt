package app.ssh

import app.game.Game
import app.util.Async
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component

@Profile("demo")
@Component
class DemoScriptService @Autowired constructor(
        private val async: Async): ScriptService {
    override fun run(game: Game, action: String) {
        async.delay(1000)
    }
}