package app.cloud.demo

import app.cloud.Node
import app.cloud.NodeApi
import app.util.Async
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Service
import java.util.concurrent.ThreadLocalRandom

@Profile("demo")
@Service
class DemoNodeApi @Autowired constructor(
        private val async: Async) : NodeApi {

    override fun create(nodeId: String): Node {
        async.delay(2000)
        val node = Node(id = nodeId, ipAddress = randomIp())
        nodes.add(node)
        return node
    }

    override fun stop(nodeId: String): Node {
        async.delay(2000)
        val node = nodes.single { it.id == nodeId }
        nodes.remove(node)
        val snapshot = node.copy(ipAddress = null)
        snapshots.add(snapshot)
        return snapshot
    }

    override fun resume(snapshotId: String): Node {
        async.delay(2000)
        val snapshot = snapshots.single { it.id == snapshotId }
        snapshots.remove(snapshot)
        val node = snapshot.copy(ipAddress = randomIp())
        nodes.add(node)
        return node
    }

    override fun deleteSnapshot(snapshotId: String) {
        async.delay(500)
        snapshots.remove(snapshots.single { it.id == snapshotId })
    }

    override fun deleteNode(nodeId: String) {
        async.delay(500)
        nodes.remove(nodes.single { it.id == nodeId })    }

    override val nodes = mutableListOf<Node>()
    override val snapshots = mutableListOf<Node>()

}

private fun  randomIp(): String = "%d.%d.%d.%d".format(Int.random(255), Int.random(255), Int.random(255), Int.random(255))

private fun Int.Companion.random(bound: Int): Int {
    return ThreadLocalRandom.current().nextInt(bound)
}
