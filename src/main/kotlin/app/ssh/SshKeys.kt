package app.ssh

import org.springframework.stereotype.Repository

import java.util.Arrays.asList

@Repository
class SshKeys {

    fun publicKey(): String {
        return "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCrN6tiWpyYVcPCeuchKcBxC1Y6HHqJuuhxsIx7oBNp7I6dxON79fyDGwNetzc9pS6RGM8g3pgjhZDtOH5GnR7SiNKCeNwJGtA6lSL19VFSwjiRK8GBC7R2pNy1L1Ybtbboq2oiFKBj7fEX/Z8rdUrQB1kPiVxQn0F5++K3qFw+qOYJ7UUItphf62uuae145XAoPoKGhF+JrAT6el6Au+Cyev1HFdS4so+VcQpEe2YslthPir17Ex0zuamwrw4jdnjhKTmwsIu4czuHpT4xhCwv24Un7bWvJxKEIkEombP+iBgcIpZVWe+2XJ2viJ2DB19tAkTohOC6JBU8X9FpTM+f5qZprV3Z5Zs05C+gPVlOzlKqOgW5VoTmX0GgpylCN+OQdHMHmD7xRy+58hEDMPpJKFxNNSI+Qjac6cZMMsQl2Ky5Fy7C+hjftSHJiFPq6h/LbNKzdr3/BDWJtC1jZr93CenEeKCdSBO6uWteNhmJUO1Fjm78xQ7pI6Zr1YK0QYii0VqeZ97Y+smgpSE1/9XdddRmCXj/NSx4BoUhGEqhdpkHgSTZWyIphAWJAdgzSViDjXqPQMvZTq76yBFqvgNr6oU45fGb99quP5hvDSmGYYopxuJAxWY7bbc9SwcN73uucv8LJWwf2o201/klUtSiqSyYUt2Vb3mg2rUXYhufiw== servee@serv.ee\n"
    }

    fun privateKey(): String {
        return "-----BEGIN RSA PRIVATE KEY-----\n" +
                "MIIJKAIBAAKCAgEAqzerYlqcmFXDwnrnISnAcQtWOhx6ibrocbCMe6ATaeyOncTj\n" +
                "e/X8gxsDXrc3PaUukRjPIN6YI4WQ7Th+Rp0e0ojSgnjcCRrQOpUi9fVRUsI4kSvB\n" +
                "gQu0dqTctS9WG7W26KtqIhSgY+3xF/2fK3VK0AdZD4lcUJ9Befvit6hcPqjmCe1F\n" +
                "CLaYX+trrmnteOVwKD6ChoRfiawE+npegLvgsnr9RxXUuLKPlXEKRHtmLJbYT4q9\n" +
                "exMdM7mpsK8OI3Z44Sk5sLCLuHM7h6U+MYQsL9uFJ+21rycShCJBKJmz/ogYHCKW\n" +
                "VVnvtlydr4idgwdfbQJE6ITguiQVPF/RaUzPn+amaa1d2eWbNOQvoD1ZTs5SqjoF\n" +
                "uVaE5l9BoKcpQjfjkHRzB5g+8UcvufIRAzD6SShcTTUiPkI2nOnGTDLEJdisuRcu\n" +
                "wvoY37UhyYhT6uofy2zSs3a9/wQ1ibQtY2a/dwnpxHignUgTurlrXjYZiVDtRY5u\n" +
                "/MUO6SOma9WCtEGIotFanmfe2PrJoKUhNf/V3XXUZgl4/zUseAaFIRhKoXaZB4Ek\n" +
                "2VsiKYQFiQHYM0lYg416j0DL2U6u+sgRar4Da+qFOOXxm/farj+Ybw0phmGKKcbi\n" +
                "QMVmO223PUsHDe97rnL/CyVsH9qNtNf5JVLUoqksmFLdlW95oNq1F2Ibn4sCAwEA\n" +
                "AQKCAgAfcRqs9hzZsK5BUZaONX+cgstz4nTyQ/uZnSU/Mpc+FW3fNR/dTL/xR42n\n" +
                "4Bdp36KcjKTE0Xu40jdaCMzPpNzbJrsZsJ4foefpSifgaQtlGjuPFSz0yLR8MSxC\n" +
                "rY2viwAq3AeieI03rKNgl3eaYHC7oQ3O15e3SneYshPhTC6cZyHPYt4Rio6S1sWw\n" +
                "gs1ULTYafdg0Yay1uHw1jzE5jMbnn8RRQIYO4OOTOu36w3pw58aTXNQM/OaY8Bhk\n" +
                "VNFQ29m453wroVAXMDoXX9X6+92tcC+UtIdvwcoZr7ytewRd0poy2Hso81oc8AcZ\n" +
                "DAp43Gq+tvL2jChEpomry6DGKI/b7XWLMpESyokXWfzhBijdMFVXLqN9w3mJAncC\n" +
                "QGLlhpdOIokfPBC0CI/WW1wGnlptcYn00nVWEvrqxSajqvcF0wMesFl6Dod4Ni/z\n" +
                "Y0G0InT3+TVPUdIWpYu+ub/M5nh5lFnWYvdhqTz/WBwhbqvuAb6uEKkVNtDG06+E\n" +
                "gyRa+UuWC6/3nDfLYnfrlSlC+BCd2RgELNNopZylcKJfSalldcVD0/SVhcc4HkmE\n" +
                "XuOc4QRciKMAhyLk7oqHlpgraNcxEcnIe10IA8Unqa2SzULTPCuu20MwwQ7Y4IQF\n" +
                "5vFJcKyMn4ihWd2w6fJ7cDY3111dutxp4TdrCA60sF122/hpgQKCAQEA1GI9P5xL\n" +
                "3ov/yDp05J+ILTCcj2wAYONlKrqXrRgbZCut0uFtmhJX42G5LPzP4y3zc3COPkMg\n" +
                "mXm2+WRc8gLZ5iJQvponP+TgX0NIqhP0sgq4hsCxaHxj0uv3BMO6ZomJdtLmn3K+\n" +
                "0Np/1O6GKRvmz5ee68lHu/70qtXx4oY4/j2H9TkAZKvvtiZWWBdxDEf9y1rmli81\n" +
                "y6kqRTR8GYoEqZ8eIzBGHbha4YbzmexD11Ph/Gql9gDyFUzHrxpxjwVjP16F0ZQJ\n" +
                "IWu+dXUBq/cbw5NiCMKKjzp6DXWkKbk9sqPu8X39xngqLS7JHFC7p7VW7U/tjsXM\n" +
                "zI705oRWiaTfywKCAQEAzmEscpmkac2hZlE1v1c9Z3SNwzfjJCUUIEFrj5RrLr3j\n" +
                "vZlOfz/uxGZ/ph78RFpNwIFW2pPN3SEXXz1k9JbSh7eocpzwIbgj4wWhXcTxC/al\n" +
                "j317C0UFXYX6SYMWOR/pQpx0tWJwNBPNLESnqaraj96Ze4FFqTw/2Pl7s13b5+D0\n" +
                "NEIMtFD52qhk7kyUZ0Hv4XwsHN8vryCq6tyh7mw6CNboV1+v7VqJXVlonwkwM/xj\n" +
                "LZCe0YQW8pqsEJFWzF2mdZ1NcTQr7mx7NIhJGCjFIGFI94i6T8o/yJgUDsBZ6ANZ\n" +
                "D8LYxxioR1hh4VufTWAQnbeuifO+sU06G8B1FtjHQQKCAQAR3aW7WSGEuhAtPsWg\n" +
                "ic8BxioVYodxnw9elc22kOWU+puntrDqmgpspUbXgvjV6oKlPl2o3Sq73mJG/mtX\n" +
                "LGg6FL7oGb2dgVpiiR1uuTkHVSGKrGEAIh6fmpvv+asPfxhiN4GQ/i0REqBYQCxm\n" +
                "/uLeqamDui3foaXhTRgI7zOSJ4jtVvsyfMgwcW8n/+jRmKi/14i4L4Z9+GAG7FBY\n" +
                "MaUMw8bcPO5ZB64VdK2/UAGw9mx3OF13oUK8CfZuQ3I0Dsb3iLMHG0XdWtvts4cN\n" +
                "eT8Lp7VCeWMAg3HK+g9yNxP6mBG8nNWhQXCC1oGycRQoGngShHu+W+NLfecaER3a\n" +
                "fFUbAoIBACFF3WqkhrNgKbNbSHUO1aGGUwy0R/Sd3b7NjU/at+UTTFJXEybE4fPc\n" +
                "KpxXxogmnv54W85lof0SxrfeKBae8lZbL9c6oiRtkauaTsfVZp9tJogZzOqmOBU5\n" +
                "8n5DmiOfl3Xbz50mv7xFrQCEGRvrpc0oMEqPLvH3+rs6jcz/RuL9i6GZPH8dxwqP\n" +
                "t6YLxqlXIhuSb3JAMWnktGmlJJ5R6JFgRJlBnhWAUUExlwJ88r6O5FaGSHSy/Rgk\n" +
                "A7WwGPRd7OZtc/zaUnLgHoT7q/0vJYjw5liyPId8rgfjLOir7lLPp145JcYDna3S\n" +
                "/ATfRrGs2UD0SjmHiKkBMGsk/juuu4ECggEBANPw9ubV12r0E2W2nOPjJ6xw+5HJ\n" +
                "gbrOkNpWywcv5fvmF96Rm4rwIAUk6My84rqQLDkFmFNn6AYprOCSGLqsKHOucTDa\n" +
                "V+debxEhttjCYjX6AMpR/MyImtmKalG+IvikOiujDu58DBOye3cUjLBeo5EvudVR\n" +
                "bJbIEnYVsv/c0+AB+y5R91DFsZpuTB5ek2gjrGdO6yA+DLkfo2oTu5QdxQGY23Td\n" +
                "8D0bkLjrxHRFxG3gIj8GhVkRW95XvRsELXXe1siKoTIjt8NRjJNrbujMn7OPRx+D\n" +
                "hPFCsM4pgnrRW7JtaEoA9IRHQr+aDNkONrGE62sd+Mu9zxa/0NJrkYQmaEY=\n" +
                "-----END RSA PRIVATE KEY-----\n"
    }

    fun fingerprint(): String {
        return "7d:e8:4f:fc:66:9f:1c:4f:ae:14:61:02:b4:a5:67:3e"
    }

    fun fingerprints(): Collection<String> {
        return asList(fingerprint(), "32:bc:36:5c:3b:8b:d0:95:0e:62:bb:b2:0b:66:d3:43", "96:49:fb:80:30:9f:02:04:a6:16:42:b4:36:bb:78:09")
    }
}
